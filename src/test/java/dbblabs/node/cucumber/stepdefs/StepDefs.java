package dbblabs.node.cucumber.stepdefs;

import dbblabs.node.NodeApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = NodeApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
