/**
 * View Models used by Spring MVC REST controllers.
 */
package dbblabs.node.web.rest.vm;
